const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const apiVersion = require('./api');

app.use(cors());
app.use(express.json());
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)

app.use('/api', apiVersion);

app.get('/', (req, res) => {
    const html = `
        <h1>Welcome API</h1>
        <div>This is all api version</div>
        <ul>
            <li><a href="/api/v1.0">ver 1.0</a></li>
            <li><a href="/api/v2.0">ver 2.0</a></li>
        </ul>
    `;
    res.send(html);
});

const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Listening on port${port}...`));