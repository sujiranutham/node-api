const Pool = require('pg').Pool
const connector = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'pilot-database',
    password: 'password',
    port: 5432,
});



module.exports = {
    connector
}
// client.connect();