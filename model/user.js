const { connector } = require('./connector');
const getAllUsers = (request, response) => {
    // const { name, email } = request.body
    connector.query('SELECT * FROM users ORDER BY firstname ASC', (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows)
    })
}

const getById = (request, response) => {
    const id = request.params.id
    connector.query(`SELECT * FROM users WHERE id = ${id} ORDER BY id ASC`, (error, results) => {
        if (error) {
            throw error
        }
        response.status(200).json(results.rows.find(o => o))
    })
}
const InsertUser = (request, response) => {
    const { firstname, lastname } = request.body
    const sql = `INSERT INTO users (firstname,lastname) VALUES ('${firstname}','${lastname}')`;
    connector.query(sql, (error, results) => {
        if (error) throw error
        response.status(200).json(true)
    })
}


module.exports = {
    getAllUsers,
    getById,
    InsertUser
}