const express = require('express');
const user = require('./../../model/user');
const router = express.Router();
const userService = require('./../../model/user');
router.get('/', function (req, res, next) {
    res.send(router.stack);
});

router.get('/user/getAll', userService.getAllUsers);

router.get('/user/getById/:id', userService.getById);

router.post('/user/Insert', userService.InsertUser);

module.exports = router;